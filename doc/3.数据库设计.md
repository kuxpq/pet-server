## 数据库设计

​        从功能需求以及原型图的分析我们可以抽取出三个数据对象，分别为用户数据、寄养消息数据以及寄养消息的图片数据。它们之间的关系为一个用户可以发布多条寄养消息，而一条寄养消息又可以包含多张图片。

#### 1、E-R图如下：

![用户实体图](assets/3.数据库设计/用户实体图.png)

---

![寄养数据实体图](assets/3.数据库设计/寄养数据实体图.png)

---

![图片实体表](assets/3.数据库设计/图片实体表.png)

---

![系统整体E-R图](assets/3.数据库设计/系统整体E-R图.png)

---



#### 2、表物理设计如下：

​      **宠物寄领养平台使用MySQL8.0数据库，数据库字符集将采用utf8 – UTF-8 Unicode编码格式，排序规则为 utf8_general_ci**



| 字段名称    | 类型          | 其他说明                      | 注释                     |
| ----------- | ------------- | ----------------------------- | ------------------------ |
| id          | char(24)      | 主键                          | pet_ + 时间戳            |
| nickname    | varchar(100)  |                               | 昵称                     |
| phone       | varchar(32)   | 非空，建立索引(Unique，BTREE) | 手机号                   |
| password    | varchar(100)  | 非空                          | 密码，双重MD5加密        |
| avatar      | varchar(255)  |                               | 头像，只存图片名称       |
| sex         | tinyint       |                               | 性别，0-未知，1-男，2-女 |
| birthday    | date          |                               | 出生日期                 |
| signature   | varchar(1200) |                               | 个性签名                 |
| create_time | datetime      |                               | 创建时间                 |
| update_time | datetime      |                               | 修改时间                 |



| 字段名称    | 类型          | 其他说明                | 注释         |
| ----------- | ------------- | ----------------------- | ------------ |
| id          | bigint        | 主键                    | 主键，时间戳 |
| title       | varchar(100)  | 非空                    | 标题         |
| pet_type    | varchar(32)   |                         | 宠物类型     |
| pet_sex     | tinyint       |                         | 宠物性别     |
| pet_month   | int           |                         | 宠物月龄     |
| link_man    | varchar(32)   | 非空                    | 联系人       |
| phone       | varchar(32)   |                         | 联系电话     |
| wechat      | varchar(32)   | 非空                    | 微信         |
| address     | varchar(100)  |                         | 领养地址     |
| desription  | varchar(1000) |                         | 详细描述     |
| user_id     | char(24)      | 外键(更新删除：CASCADE) | user表id外键 |
| create_time | datetime      |                         | 创建时间     |
| update_time | datetime      |                         | 修改时间     |



| 字段名称         | 类型         | 其他说明                | 注释                  |
| ---------------- | ------------ | ----------------------- | --------------------- |
| id               | bigint       | 主键                    | 主键                  |
| filename         | varchar(255) |                         | 图片文件名            |
| bailment_data_id | bigint       | 外键(更新删除：CASCADE) | bailment_data表id外键 |
| create_time      | datetime     |                         | 创建时间              |
| update_time      | datetime     |                         | 修改时间              |