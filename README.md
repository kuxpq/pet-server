##pet-server 宠物寄领养平台后端项目（项目开发文档在doc目录下）

### 1、技术栈

1. JDK11
2. Maven 3.6.1
3. SpringBoot 2.6.4
4. mysql-connector-java SpringBoot管理的版本
5. druid-spring-boot-starter 1.2.6
6. mybatis-spring-boot-starter 2.1.3
7. java-jwt 3.18.3
8. pagehelper-spring-boot-starter 1.4.2
9. lombok SpringBoot管理的版本
10. mapstruct 1.4.2.Final
11. mapstruct-processor 1.4.2.Final
12. springfox-swagger2 3.0.0
13. swagger-bootstrap-ui 1.9.6

---

### 2、项目配置（该文件的图片放在assets/README目录下）

####2.1 application.yml文件配置，可自行查看

####2.2 数据库配置

1. 数据库使用MySQL8.0版本
   * 数据库字符编码：utf8 – UTF-8 Unicode
   * 排序规则：utf8_general_ci
2. 数据库sql文件放在sql目录下
3. 上传的文件放在file目录下

4. 文件上传的路径和前端访问图片文件的路径在util包下的PathUtils.java配置

---

#### 2.3 jwt拦截器配置

1. jwt拦截器类为interceptor/JWTInterceptor.java，在该类中设置好允许跨域的域

