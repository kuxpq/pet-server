package com.xpq.petserver.util;

import java.text.SimpleDateFormat;

/**
 * 获取时间工具类
 * @author XPQ
 * @date 2022-04-23
 */
public class GetTimeUtils {
    /**
     * 获取当前时间
     * @return 当前时间字符串：2022-01-01 12:00:00
     */
    public static String getCurrentTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(System.currentTimeMillis());
    }
}
