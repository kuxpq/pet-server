package com.xpq.petserver.util;

import java.io.File;

/**
 * 获取路径的工具类，该类写得不太好，比较不合理
 * @author XPQ
 * @date 2022-04-01
 */
public class PathUtils {
    /**
     * 文件访问的逻辑路径
     * @return
     */
    public static String getUploadLogicalPath() {
        return "/file/**";
    }

    /**
     * 获取文件上传的路径，项目根路径下的file文件夹，或者与jar包同目录的file文件夹
     * @return 文件上传的路径
     */
    public static String getUploadPath() {
//        return "/home/xiepeiquan/projects/file/";
        return System.getProperty("user.dir") + File.separator + "file" + File.separator ;
    }

    /**
     * 获取头像文件上传的逻辑位置
     * @return
     */
    public static String getAvatarLogicalPath() {
        return "http://localhost:8081/file/avatar/";
    }

    /**
     * 获取头像上传的物理位置
     * @return 头像上传的位置
     */
    public static String getAvatarPhysicalPath() {
//        return "/home/xiepeiquan/projects/file/avatar/";
        return System.getProperty("user.dir") + File.separator + "file" + File.separator + "avatar" + File.separator;
    }

    /**
     * 获取图片的逻辑路径
     * @return
     */
    public static String getPictureLogicalPath() {
        return "http://localhost:8081/file/picture/";
    }

    /**
     * 获取图片的物理路径
     * @return
     */
    public static String getPicturePhysicalPath() {
//        return "/home/xiepeiquan/projects/file/picture/";
        return System.getProperty("user.dir") + File.separator + "file" + File.separator + "picture" + File.separator;
    }
}
