package com.xpq.petserver.util.mapstruct;

import com.xpq.petserver.entity.DTO.SearchBailmentDataDTO;
import com.xpq.petserver.entity.DTO.PublicFosterageDTO;
import com.xpq.petserver.entity.request.bailmentDataController.SearchBailmentDataRequest;
import com.xpq.petserver.entity.request.bailmentDataController.PublicFosterageRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 与BailmentData实体相关的DTO、VO、DO转换器
 * @author XPQ
 * @date 2022-04-01
 */
@Mapper
public interface BailmentDataConverter {
    BailmentDataConverter INSTANCT = Mappers.getMapper(BailmentDataConverter.class);

    PublicFosterageDTO toPublicFosterageDTO(PublicFosterageRequest publicFosterageRequest);
    SearchBailmentDataDTO toGetBailmentDataDTO(SearchBailmentDataRequest searchBailmentDataRequest);
}
