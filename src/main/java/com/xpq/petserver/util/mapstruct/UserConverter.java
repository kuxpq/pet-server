package com.xpq.petserver.util.mapstruct;
import com.xpq.petserver.entity.DO.User;
import com.xpq.petserver.entity.DTO.*;
import com.xpq.petserver.entity.VO.UserInfoVO;
import com.xpq.petserver.entity.request.userController.LoginRegisterRequest;
import com.xpq.petserver.entity.request.userController.UpdatePasswordRequest;
import com.xpq.petserver.entity.request.userController.UpdatePhoneRequest;
import com.xpq.petserver.entity.request.userController.UserBasicInfoRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 与User实体相关的DTO、VO、DO转换器
 * @author XPQ
 * @date 2022-04-01
 */
@Mapper
public interface UserConverter {
    UserConverter INSTANCT = Mappers.getMapper(UserConverter.class);

    RegisterUserDTO toRegisterUserDTO(LoginRegisterRequest loginRegisterRequest);
    LoginDTO toLoginDTO(LoginRegisterRequest loginRegisterRequest);
    UserInfoVO toUserInfoVO(User user);
    UserBasicInfoDTO toUserBasicInfoDTO(UserBasicInfoRequest userBasicInfo);
    UpdatePasswordDTO toUpdatePasswordDTO(UpdatePasswordRequest updatePasswordRequest);
    UpdatePhoneDTO toUpdatePhoneDTO(UpdatePhoneRequest updatePhoneRequest);
}
