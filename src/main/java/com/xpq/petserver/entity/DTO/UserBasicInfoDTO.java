package com.xpq.petserver.entity.DTO;

import lombok.Data;

/**
 * 修改用户的基本信息业务参数
 * @author XPQ
 * @date 2022-04-01
 */
@Data
public class UserBasicInfoDTO {
    private String id;
    private String nickname;
    private Byte sex;
    private String birthday;
    private String signature;
    private String updateTime;
}
