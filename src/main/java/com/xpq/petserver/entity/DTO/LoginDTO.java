package com.xpq.petserver.entity.DTO;

import lombok.Data;

/**
 * 用户登录业务参数
 * @author XPQ
 * @date 2022-04-24
 */
@Data
public class LoginDTO {
    // 电话号码
    private String phone;
    // 密码
    private String password;
}
