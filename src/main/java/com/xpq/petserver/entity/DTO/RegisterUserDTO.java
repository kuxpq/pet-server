package com.xpq.petserver.entity.DTO;

import lombok.Data;

/**
 * 注册用户业务参数
 * @author XPQ
 * @date 2022-04-01
 */
@Data
public class RegisterUserDTO {
    // 用户id
    private String id;
    // 手机号码
    private String phone;
    // 密码
    private String password;
    // 创建时间
    private String createTime;
    // 修改时间
    private String updateTime;
}
