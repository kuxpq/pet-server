package com.xpq.petserver.entity.DTO;

import lombok.Data;

/**
 * 修改用户手机号业务参数
 * @author XPQ
 * @date 2022-05-04
 */
@Data
public class UpdatePhoneDTO {
    private String id;
    private String newPhone;
    private String password;
    private String updateTime;
}
