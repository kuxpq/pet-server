package com.xpq.petserver.entity.DTO;

import lombok.Data;

/**
 * 获取寄养信息业务参数
 * @author XPQ
 * @date 2022-05-03
 */
@Data
public class SearchBailmentDataDTO {
    private Integer pageNum;
    private Integer pageSize;
    private String keyWord;
    private String petType;
    private Byte petSex;
    private Integer minPetMonth;
    private Integer maxPetMonth;
}
