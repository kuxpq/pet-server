package com.xpq.petserver.entity.DTO;

import lombok.Data;

/**
 * 修改密码业务参数
 * @author XPQ
 * @date 2022-05-03
 */
@Data
public class UpdatePasswordDTO {
    private String id;
    private String originalPassword;
    private String newPassword;
    private String updateTime;
}
