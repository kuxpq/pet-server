package com.xpq.petserver.entity.DTO;

import lombok.Data;

/**
 * 发布寄养消息业务参数
 * @author XPQ
 * @date 2022-05-03
 */
@Data
public class PublicFosterageDTO {
    private Long id;
    private String title;
    private String petType;
    private Byte petSex;
    private Integer petMonth;
    private String linkman;
    private String wechat;
    private String phone;
    private String address;
    private String description;
    private String userId;
    private String createTime;
    private String updateTime;
}
