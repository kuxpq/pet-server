package com.xpq.petserver.entity.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 响应数据格式
 * @author XPQ
 * @date 2022-03-07
 * */
@ApiModel("通用响应数据格式")
@Data
public class GeneralResponse {
    @ApiModelProperty("响应状态码")
    private Integer code;
    @ApiModelProperty("提示消息")
    private String msg;
    @ApiModelProperty("响应数据")
    private Object data;
}
