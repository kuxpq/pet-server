package com.xpq.petserver.entity.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 登录携带token响应数据
 * @author XPQ
 * @date 2022-04-23
 */
@ApiModel("登录携带token响应数据格式")
@Data
public class LoginResponse {
    @ApiModelProperty("响应状态码")
    private Integer code;
    @ApiModelProperty("提示消息")
    private String msg;
    @ApiModelProperty("响应数据")
    private Object data;
    @ApiModelProperty("用户token")
    private String token;
}
