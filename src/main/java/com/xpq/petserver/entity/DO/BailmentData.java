package com.xpq.petserver.entity.DO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 寄养数据实体类
 * @author XPQ
 * @date 2022-04-21
 */
@ApiModel("寄养数据实体类")
@Data
public class BailmentData {
    @ApiModelProperty("主键，13位时间戳")
    private Long id;
    @ApiModelProperty("寄养信息标题，最大长度100")
    private String title;
    @ApiModelProperty("宠物类型，最大长度32")
    private String petType;
    @ApiModelProperty("宠物性别，0-未知，1-雄性，2-雌性")
    private Byte petSex;
    @ApiModelProperty("宠物年龄，几个月")
    private Integer petMonth;
    @ApiModelProperty("联系人，最大长度32")
    private String linkman;
    @ApiModelProperty("联系电话，最大长度32")
    private String phone;
    @ApiModelProperty("微信，最大长度32")
    private String wechat;
    @ApiModelProperty("地址，最大长度100")
    private String address;
    @ApiModelProperty("详细描述信息，最大长度1000")
    private String description;
    @ApiModelProperty("数据状态，未被领养-0，已被领养-1")
    private Byte status;
    @ApiModelProperty("外键，用户id，最大长度24")
    private String userId;
    @ApiModelProperty("创建时间，2022-01-01 12:00:00")
    private String createTime;
    @ApiModelProperty("更新时间，2022-01-01 12:00:00")
    private String updateTime;
    @ApiModelProperty("寄养数据图片")
    private List<Picture> pictures;

}
