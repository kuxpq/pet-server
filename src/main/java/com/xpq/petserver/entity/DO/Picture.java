package com.xpq.petserver.entity.DO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 图片实体类
 * @author XPQ
 * @date 2022-04-21
 */
@ApiModel("图片实体类")
@Data
public class Picture {
    @ApiModelProperty("主键，13位时间戳")
    private Long id;
    @ApiModelProperty("图片名称，最大长度255")
    private String filename;
    @ApiModelProperty("外键，寄养数据id")
    private Long bailmentDataId;
    @ApiModelProperty("创建时间，2022-01-01 12:00:00")
    private String createTime;
    @ApiModelProperty("更新时间，2022-01-01 12:00:00")
    private String updateTime;
}
