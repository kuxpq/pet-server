package com.xpq.petserver.entity.DO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 用户表实体类
 * @author XPQ
 * @date 2022-04-21
 * */
@ApiModel("用户实体类")
@Data
public class User {
    @ApiModelProperty("用户id，不作为登录账号使用，最大长度24")
    private String id;
    @ApiModelProperty("用户昵称，最大长度100")
    private String nickname;
    @ApiModelProperty("用户手机号码，作为登录账号使用，最大长度32")
    private String phone;
    @ApiModelProperty("用户密码，最大长度100")
    private String password;
    @ApiModelProperty("用户头像，最大长度255")
    private String avatar;
    @ApiModelProperty("用户性别，未知-0，男-1，女-2")
    private Byte sex;
    @ApiModelProperty("用户出生日期，2000-01-01")
    private String birthday;
    @ApiModelProperty("个性签名，最大长度1200")
    private String signature;
    @ApiModelProperty("账号状态，正常-0，冻结-1")
    private Byte status;
    @ApiModelProperty("账号删除标志，未删除-0，已删除-1")
    private Byte delFlag;
    @ApiModelProperty("创建时间，2022-01-01 12:00:00")
    private String createTime;
    @ApiModelProperty("修改时间，2022-01-01 12:00:00")
    private String updateTime;
    @ApiModelProperty("发布的寄养数据")
    private List<BailmentData> bailmentDatas;

}
