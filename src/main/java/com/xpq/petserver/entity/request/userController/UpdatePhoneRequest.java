package com.xpq.petserver.entity.request.userController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 修改手机号码请求参数
 * @author XPQ
 * @date 2022-05-04
 */
@ApiModel("修改手机号请求参数")
@Data
public class UpdatePhoneRequest {
    @ApiModelProperty(value = "新手机号码", required = true)
    private String newPhone;
    @ApiModelProperty(value = "密码", required = true)
    private String password;
}
