package com.xpq.petserver.entity.request.userController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 修改用户基本信息的请求参数
 * @author XPQ
 * @date 2022-04-01
 */
@ApiModel("修改用户基本信息请求参数")
@Data
public class UserBasicInfoRequest {
    @ApiModelProperty(value = "用户昵称")
    private String nickname;
    @ApiModelProperty(value = "用户性别")
    private Byte sex;
    @ApiModelProperty(value = "用户出生日期")
    private String birthday;
    @ApiModelProperty(value = "用户个性签名")
    private String signature;
}
