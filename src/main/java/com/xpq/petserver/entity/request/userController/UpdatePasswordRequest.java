package com.xpq.petserver.entity.request.userController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 修改用户密码请求参数
 * @author XPQ
 * @date 2022-05-03
 */
@ApiModel("修改密码请求参数")
@Data
public class UpdatePasswordRequest {
    @ApiModelProperty(value = "原密码", required = true)
    private String originalPassword;
    @ApiModelProperty(value = "新密码", required = true)
    private String newPassword;
}
