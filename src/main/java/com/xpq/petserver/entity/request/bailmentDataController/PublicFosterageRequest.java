package com.xpq.petserver.entity.request.bailmentDataController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 发布寄养消息时的请求参数
 * @author XPQ
 * @date 2022-05-03
 */
@ApiModel("发布寄养消息数据请求参数")
@Data
public class PublicFosterageRequest {
    @ApiModelProperty(value = "标题", required = true)
    private String title;
    @ApiModelProperty(value = "宠物类型")
    private String petType;
    @ApiModelProperty(value = "宠物性别，0-未知，1-雄性，2-雌性")
    private Byte petSex;
    @ApiModelProperty("宠物年龄，几个月")
    private Integer petMonth;
    @ApiModelProperty(value = "联系人", required = true)
    private String linkman;
    @ApiModelProperty(value = "微信号", required = true)
    private String wechat;
    @ApiModelProperty(value = "联系电话")
    private String phone;
    @ApiModelProperty(value = "联系地址")
    private String address;
    @ApiModelProperty(value = "详细描述")
    private String description;
}
