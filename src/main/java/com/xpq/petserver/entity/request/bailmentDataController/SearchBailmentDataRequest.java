package com.xpq.petserver.entity.request.bailmentDataController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 获取寄养数据
 * @author XPQ
 * @date 2022-05-03
 */
@ApiModel("获取寄养数据请求参数")
@Data
public class SearchBailmentDataRequest {
    @ApiModelProperty("第几页")
    private Integer pageNum;
    @ApiModelProperty("每页数据条数")
    private Integer pageSize;
    @ApiModelProperty("搜索关键词")
    private String keyWord;
    @ApiModelProperty("宠物类型")
    private String petType;
    @ApiModelProperty("宠物性别，整数类型，0-未知，1-雄性，2-雌性")
    private Byte petSex;
    @ApiModelProperty("宠物最小年龄")
    private Integer minPetMonth;
    @ApiModelProperty("宠物最大年龄")
    private Integer maxPetMonth;
}
