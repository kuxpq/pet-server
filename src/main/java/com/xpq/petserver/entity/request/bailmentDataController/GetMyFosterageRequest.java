package com.xpq.petserver.entity.request.bailmentDataController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 获取我的寄养数据请求参数
 * @author XPQ
 * @date 2022-05-03
 */
@ApiModel("获取用户寄养数据请求参数")
@Data
public class GetMyFosterageRequest {
    @ApiModelProperty("第几页")
    private Integer pageNum;
    @ApiModelProperty("每页数据条数")
    private Integer pageSize;
}
