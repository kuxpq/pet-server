package com.xpq.petserver.entity.request.userController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户注册接口参数
 * @author XPQ
 * @date 2022-04-23
 */
@ApiModel("用户注册请求参数")
@Data
public class LoginRegisterRequest {
    @ApiModelProperty(value = "注册手机号，最大长度32字符，不能包含空格", required = true)
    private String phone;
    @ApiModelProperty(value = "注册密码，最大长度100字符", required = true)
    private String password;
}
