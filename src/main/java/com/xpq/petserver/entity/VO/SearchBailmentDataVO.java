package com.xpq.petserver.entity.VO;

import lombok.Data;

/**
 * 查找寄养数据返回的数据
 * @author XPQ
 * @date 2022-05-03
 */
@Data
public class SearchBailmentDataVO {
    private Long id;
    private String title;
    private String userNickname;
    private String createTime;
    private String picture;
}
