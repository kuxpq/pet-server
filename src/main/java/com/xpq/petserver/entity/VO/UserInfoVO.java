package com.xpq.petserver.entity.VO;

import lombok.Data;

/**
 * 用户登录之后返回的用户信息
 * @author XPQ
 * @date 2022-04-01
 */
@Data
public class UserInfoVO {
    private String id;
    private String nickname;
    private String phone;
    private String avatar;
    private Byte sex;
    private String birthday;
    private String signature;
}
