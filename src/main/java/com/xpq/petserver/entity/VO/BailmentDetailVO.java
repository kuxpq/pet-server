package com.xpq.petserver.entity.VO;

import lombok.Data;

import java.util.List;

/**
 * 寄养信息详情数据
 * @author XPQ
 * @date 2022-05-03
 */
@Data
public class BailmentDetailVO {
    private Long id;
    private String title;
    private String petType;
    private Byte petSex;
    private Integer petMonth;
    private String linkman;
    private String phone;
    private String wechat;
    private String address;
    private String description;
    private String createTime;
    private String updateTime;
    private List<String> pictures;
}
