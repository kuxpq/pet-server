package com.xpq.petserver.entity.VO;

import lombok.Data;

/**
 * 我的寄养数据信息
 * @author XPQ
 * @date 2022-05-03
 */
@Data
public class MyFosterageVO {
    private Long id;
    private String title;
    private String petType;
    private Byte petSex;
    private Integer petMonth;
    private String createTime;
    private String picture;
}
