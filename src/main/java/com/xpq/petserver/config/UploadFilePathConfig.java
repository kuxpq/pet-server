package com.xpq.petserver.config;

import com.xpq.petserver.util.PathUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 文件上传路径配置类
 * @author XPQ
 * @date 2022-04-01
 */
@Configuration
public class UploadFilePathConfig implements WebMvcConfigurer {
    // 头像访问位置
    private String staticAccessPath = PathUtils.getUploadLogicalPath();
    private String uploadFolder = PathUtils.getUploadPath();

    /**
     * Add handlers to serve static resources such as images, js, and, css
     * files from specific locations under web application root, the classpath,
     * and others.
     *
     * @param registry
     * @see ResourceHandlerRegistry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(staticAccessPath).addResourceLocations("file:" + uploadFolder);
    }
}
