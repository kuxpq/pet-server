package com.xpq.petserver.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

/**
 * 文件上传配置类(已废弃，文件大小已在配置文件配置好)
 * @author XPQ
 * @date 2022-04-01
 */
//@Configuration
public class UploadFileConfig {
    //@Value("${file.uploadFolder}")
//    private String uploadFolder;

//    @Bean
//    MultipartConfigElement multipartConfigElement() {
//        MultipartConfigFactory factory = new MultipartConfigFactory();
//        // 单文件最大
//        factory.setMaxFileSize(DataSize.ofMegabytes(30));
//        // 设置总上传文件数据大小
//        factory.setMaxRequestSize(DataSize.ofMegabytes(120));
//        return factory.createMultipartConfig();
//    }
}
