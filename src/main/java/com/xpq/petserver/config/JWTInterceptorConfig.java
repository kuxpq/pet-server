package com.xpq.petserver.config;

import com.xpq.petserver.interceptors.JWTInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class JWTInterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new JWTInterceptor())
                .addPathPatterns("/**")//拦截所有路径  /**
                .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/doc.html/**")// 放行api文档
                .excludePathPatterns("/","/index.html","/init.css","favicon.ico", "/js/**", "/css/**", "/json/**", "/img/**")
                .excludePathPatterns("/file/**")// 放行上传的文件
                .excludePathPatterns("/user/login","/user/register")// 放行登录、注册
                .excludePathPatterns("/bailmentData/searchBailmentData","/bailmentData/getBailmentDetail");// 放行搜索寄养消息
    }
}
