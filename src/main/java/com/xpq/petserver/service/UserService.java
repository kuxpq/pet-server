package com.xpq.petserver.service;

import com.xpq.petserver.entity.DO.User;
import com.xpq.petserver.entity.DTO.*;
import com.xpq.petserver.entity.response.GeneralResponse;
import com.xpq.petserver.entity.response.LoginResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 用户表相关服务
 * @author XPQ
 * @date 2022-04-23
 */
public interface UserService {
    /**
     * 注册用户服务
     * @param registerUserDTO 新用户信息
     * @return 注册成功失败提示信息
     */
    GeneralResponse registerUser(RegisterUserDTO registerUserDTO);

    /**
     * 用户登录服务
     * @param loginDTO 用户登录信息
     * @return
     */
    LoginResponse login(LoginDTO loginDTO);

    /**
     * 判断用户是否已登录
     * @param id 用户id
     * @return 用户头像地址
     */
    GeneralResponse isLogin(String id);

    /**
     * 上传修改用户头像
     * @param avatar 用户上传的头像文件
     * @param userId 用户的id
     * @return
     */
    GeneralResponse uploadAvatar(MultipartFile avatar, String userId) throws IOException;

    /**
     * 修改用户基本信息
     * @param userBasicInfoDTO 用户基本信息
     * @return
     */
    GeneralResponse updateUserBasicInfo(UserBasicInfoDTO userBasicInfoDTO);

    /**
     * 修改用户密码
     * @param updatePasswordDTO 用户id和原、新密码
     * @return
     */
    GeneralResponse updatePassword(UpdatePasswordDTO updatePasswordDTO);

    /**
     * 修改用户手机号码
     * @param updatePhoneDTO 用户id、新手机号、密码
     * @return
     */
    GeneralResponse updatePhone(UpdatePhoneDTO updatePhoneDTO);

}
