package com.xpq.petserver.service;

import com.xpq.petserver.entity.DTO.PublicFosterageDTO;
import com.xpq.petserver.entity.DTO.SearchBailmentDataDTO;
import com.xpq.petserver.entity.response.GeneralResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 寄养数据相关业务
 * @author XPQ
 * @date 2022-04-01
 */
public interface BailmentDataService {
    /**
     * 发布寄养消息业务
     * @param multipartFiles 上传的图片
     * @param publicFosterageDTO 寄养信息
     * @return
     */
    GeneralResponse publicFosterage(List<MultipartFile> multipartFiles, PublicFosterageDTO publicFosterageDTO);

    /**
     * 获取用户寄养数据
     * @param userId 用户id
     * @return
     */
    GeneralResponse getMyFosterage(String userId, Integer pageNum, Integer pageSize);

    /**
     * 获取寄养消息的详情信息
     * @param id 寄养消息id
     * @return
     */
    GeneralResponse getBailmentDetail(Long id);

    /**
     * 删除寄养数据
     * @param id 寄养数据id
     * @param userId 用户id
     * @return
     */
    GeneralResponse deleteBailmentData(Long id, String userId);

    /**
     * 搜索寄养数据业务
     * @param searchBailmentDataDTO 搜索条件
     * @return
     */
    GeneralResponse searchBailmentData(SearchBailmentDataDTO searchBailmentDataDTO);
}
