package com.xpq.petserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

/**
 * 宠物寄领养平台
 * @author XPQ
 * @date 2022-2-26
 * */
@SpringBootApplication
@MapperScan("com.xpq.petserver.mapper")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
