package com.xpq.petserver.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xpq.petserver.entity.DTO.SearchBailmentDataDTO;
import com.xpq.petserver.entity.DTO.PublicFosterageDTO;
import com.xpq.petserver.entity.request.bailmentDataController.SearchBailmentDataRequest;
import com.xpq.petserver.entity.request.bailmentDataController.GetMyFosterageRequest;
import com.xpq.petserver.entity.request.bailmentDataController.PublicFosterageRequest;
import com.xpq.petserver.entity.response.GeneralResponse;
import com.xpq.petserver.service.BailmentDataService;
import com.xpq.petserver.util.JWTUtils;
import com.xpq.petserver.util.mapstruct.BailmentDataConverter;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 寄养数据相关请求接口
 *
 * @author XPQ
 * @date 2022-05-03
 */
@CrossOrigin
@RestController
@RequestMapping("/bailmentData")
public class BailmentDataController {
    @Autowired
    private BailmentDataService bailmentDataService;

    /**
     * 发布寄养消息接口
     * @param multipartFiles
     * @param data
     * @return
     */
    @ApiOperation("发布寄养消息接口")
    @PostMapping("/publicFosterage")
    public GeneralResponse publicFosterage(@ApiParam("寄养图片") @RequestPart("files") List<MultipartFile> multipartFiles,
                                           @ApiParam("寄养数据") @RequestPart("data") String data,
                                           HttpServletRequest request) throws JsonProcessingException {
        ObjectMapper mapperBuilder = new ObjectMapper();
        PublicFosterageRequest publicFosterageRequest = mapperBuilder.readValue(data, PublicFosterageRequest.class);
        PublicFosterageDTO publicFosterageDTO = BailmentDataConverter.INSTANCT.toPublicFosterageDTO(publicFosterageRequest);
        String token = request.getHeader("token");
        publicFosterageDTO.setUserId(JWTUtils.getUserId(token));
        return bailmentDataService.publicFosterage(multipartFiles, publicFosterageDTO);
    }

    /**
     * 获取用户寄养数据接口
     * @param request
     * @return
     */
    @ApiOperation("获取用户寄养数据接口")
    @PostMapping("/getMyFosterage")
    public GeneralResponse getMyFosterage(@ApiParam("获取用户寄养数据请求参数") @RequestBody GetMyFosterageRequest getMyFosterageRequest,
                                          HttpServletRequest request) {

        String token = request.getHeader("token");
        return bailmentDataService.getMyFosterage(JWTUtils.getUserId(token), getMyFosterageRequest.getPageNum(), getMyFosterageRequest.getPageSize());
    }

    /**
     * 获取寄养信息详情接口
     * @param id 寄养数据id
     * @return
     */
    @ApiOperation("获取寄养信息详情接口")
    @GetMapping("/getBailmentDetail")
    public GeneralResponse getBailmentDetail(@ApiParam("寄养数据id") @RequestParam("id") Long id) {
        return bailmentDataService.getBailmentDetail(id);
    }

    /**
     * 删除寄养数据接口
     * @param id 寄养数据id
     * @param request 获取用户id
     * @return
     */
    @ApiOperation("删除寄养数据接口")
    @DeleteMapping("/deleteBailmentData")
    public GeneralResponse deleteBailmentDetail(@ApiParam("寄养数据id，拼接在url上") @RequestParam("id") Long id, HttpServletRequest request) {
        String token = request.getHeader("token");
        return bailmentDataService.deleteBailmentData(id, JWTUtils.getUserId(token));
    }

    /**
     * 搜索寄养数据接口
     * @param searchBailmentDataRequest 查询条件
     * @return
     */
    @ApiOperation("搜索寄养数据接口")
    @PostMapping("/searchBailmentData")
    public GeneralResponse searchBailmentData(@RequestBody SearchBailmentDataRequest searchBailmentDataRequest) {

        SearchBailmentDataDTO searchBailmentDataDTO = BailmentDataConverter.INSTANCT.toGetBailmentDataDTO(searchBailmentDataRequest);
        return bailmentDataService.searchBailmentData(searchBailmentDataDTO);
    }
}
