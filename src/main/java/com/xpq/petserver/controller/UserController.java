package com.xpq.petserver.controller;

import com.xpq.petserver.entity.DTO.*;
import com.xpq.petserver.entity.request.userController.LoginRegisterRequest;
import com.xpq.petserver.entity.request.userController.UpdatePasswordRequest;
import com.xpq.petserver.entity.request.userController.UpdatePhoneRequest;
import com.xpq.petserver.entity.request.userController.UserBasicInfoRequest;
import com.xpq.petserver.entity.response.GeneralResponse;
import com.xpq.petserver.entity.response.LoginResponse;
import com.xpq.petserver.service.UserService;
import com.xpq.petserver.util.JWTUtils;
import com.xpq.petserver.util.mapstruct.UserConverter;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 用户表相关接口
 *
 * @author XPQ
 * @date 2022-04-22
 */
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 用户注册接口
     *
     * @param loginRegisterRequest 用户注册信息
     * @return
     */
    @ApiOperation("用户注册接口")
    @PostMapping("/register")
    public GeneralResponse register(@ApiParam("用户注册信息") @RequestBody LoginRegisterRequest loginRegisterRequest) {
        RegisterUserDTO registerUserDTO = UserConverter.INSTANCT.toRegisterUserDTO(loginRegisterRequest);
        return userService.registerUser(registerUserDTO);
    }

    /**
     * 用户登录接口
     *
     * @param loginRegisterRequest 用户登录信息
     * @return
     */
    @ApiOperation("用户登录接口")
    @PostMapping("/login")
    public LoginResponse login(@ApiParam("用户登录信息") @RequestBody LoginRegisterRequest loginRegisterRequest) {
        LoginDTO loginDTO = UserConverter.INSTANCT.toLoginDTO(loginRegisterRequest);
        return userService.login(loginDTO);
    }

    /**
     * 判断用户是否一已登录
     *
     * @param request 获取用户token
     * @return 返回用户头像
     */
    @ApiOperation("用户是否已登录判断接口")
    @PostMapping("/isLogin")
    public GeneralResponse isLogin(HttpServletRequest request) {
        GeneralResponse generalResponse = new GeneralResponse();
        generalResponse.setCode(200);
        return generalResponse;
    }

    /**
     * 获取用户信息
     *
     * @param request 获取用户token
     * @return 返回用户头像
     */
    @ApiOperation("获取用户信息接口")
    @PostMapping("/getUserInfo")
    public GeneralResponse getUserInfo(HttpServletRequest request) {
        String token = request.getHeader("token");
        return userService.isLogin(JWTUtils.getUserId(token));
    }

    /**
     * 头像上传接口
     *
     * @param avatar             头像文件
     * @param httpServletRequest 获取token用
     * @return 用户新的头像地址
     * @throws IOException
     */
    @ApiOperation("头像上传接口")
    @PostMapping("/uploadAvatar")
    public GeneralResponse uploadAvatar(@ApiParam("头像文件") @RequestPart("avatarFile") MultipartFile avatar,
                                        HttpServletRequest httpServletRequest) throws IOException {
        String token = httpServletRequest.getHeader("token");
        return userService.uploadAvatar(avatar, JWTUtils.getUserId(token));
    }

    /**
     * 修改用户基本信息
     * @param userBasicInfo 用户基本信息
     * @param request       获取token来获取用户id
     * @return
     */
    @ApiOperation("修改用户基本信息接口")
    @PostMapping("/updateUserBasicInfo")
    public GeneralResponse updateUserBasicInfo(@ApiParam("用户基本信息") @RequestBody UserBasicInfoRequest userBasicInfo,
                                               HttpServletRequest request) {
        UserBasicInfoDTO userBasicInfoDTO = UserConverter.INSTANCT.toUserBasicInfoDTO(userBasicInfo);
        String token = request.getHeader("token");
        userBasicInfoDTO.setId(JWTUtils.getUserId(token));
        return userService.updateUserBasicInfo(userBasicInfoDTO);
    }

    /**
     * 修改用户密码接口
     * @param updatePasswordRequest
     * @param request
     * @return
     */
    @ApiOperation("修改用户密码接口")
    @PostMapping("/updatePassword")
    public GeneralResponse updatePassword(@ApiParam("原新密码") @RequestBody UpdatePasswordRequest updatePasswordRequest,
                                          HttpServletRequest request) {
        UpdatePasswordDTO updatePasswordDTO = UserConverter.INSTANCT.toUpdatePasswordDTO(updatePasswordRequest);
        String token = request.getHeader("token");
        updatePasswordDTO.setId(JWTUtils.getUserId(token));
        return userService.updatePassword(updatePasswordDTO);
    }

    @ApiOperation("修改用户手机号接口")
    @PostMapping("/updatePhone")
    public GeneralResponse updatePhone(@ApiParam("新手机号和密码") @RequestBody UpdatePhoneRequest updatePhoneRequest,
                                       HttpServletRequest request) {
        UpdatePhoneDTO updatePhoneDTO = UserConverter.INSTANCT.toUpdatePhoneDTO(updatePhoneRequest);
        String token = request.getHeader("token");
        updatePhoneDTO.setId(JWTUtils.getUserId(token));
        return userService.updatePhone(updatePhoneDTO);
    }
}
