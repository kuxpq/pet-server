package com.xpq.petserver.mapper;

import com.xpq.petserver.entity.DO.Picture;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author XPQ
 * @date 2022-04-01
 */
@Repository
public interface PictureMapper {
    /**
     * 批量插入图片
     * @param pictures
     */
    void insertPicture(@Param("pictures") List<Picture> pictures);

    /**
     * 获取寄养数据的第一张图片的名称
     * @param bailmentDataId 寄养数据id
     * @return
     */
    @Select("select filename from picture where bailment_data_id = #{bailmentDataId} limit 1")
    String selectFistFilename(@Param("bailmentDataId") Long bailmentDataId);

    /**
     * 获取某条寄养信息的全部图片
     * @param bailmentDataId
     * @return
     */
    @Select("select filename from picture where bailment_data_id = #{bailmentDataId}")
    List<String> selectFilenames(@Param("bailmentDataId") Long bailmentDataId);

}
