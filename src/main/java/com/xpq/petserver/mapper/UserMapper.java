package com.xpq.petserver.mapper;

import com.xpq.petserver.entity.DO.User;
import com.xpq.petserver.entity.DTO.RegisterUserDTO;
import com.xpq.petserver.entity.DTO.UpdatePasswordDTO;
import com.xpq.petserver.entity.DTO.UpdatePhoneDTO;
import com.xpq.petserver.entity.DTO.UserBasicInfoDTO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    /**
     * 查询所有用户数据
     * @return 所有用户数据
     */
    @Select("select * from user")
    List<User> selectAll();

    /**
     * 注册用户
     * @param registerUserDTO 用户数据
     */
    @Insert("insert into user (id, phone, password, create_time, update_time) "
            + "values (#{id},#{phone},#{password},#{createTime},#{updateTime})")
    void createUser(RegisterUserDTO registerUserDTO);

    /**
     * 根据电话号码查询用户
     * @param phone 包含用户电话号码
     * @return 对应的用户数据
     */
    @Select("select * from user where phone=#{phone}")
    User selectUserByPhone(@Param("phone") String phone);

    /**
     * 根据用户id查询用户
     * @param id 用户id
     * @return 对应的用户数据
     */
    @Select("select * from user where id=#{id}")
    User selectUserById(@Param("id") String id);

    /**
     * 修改用户头像
     * @param id 用户id
     * @param avatar 用户头像文件名
     */
    @Update("update user set avatar=#{avatar} where id=#{id}")
    void updateAvatar(@Param("id") String id, @Param("avatar") String avatar);

    /**
     * 修改用户基本信息
     * @param userBasicInfo 用户基本信息
     */
    @Update("update user set nickname = #{nickname}, sex = #{sex}, birthday = #{birthday}, signature = #{signature}, "
            + "update_time = #{updateTime} where id = #{id}")
    void updateUserBasicInfo(UserBasicInfoDTO userBasicInfo);

    /**
     * 修改用户密码
     * @param updatePasswordDTO 用户id、新密码、更新时间
     */
    @Update("update user set password = #{newPassword}, update_time = #{updateTime} where id = #{id}")
    void updatePassword(UpdatePasswordDTO updatePasswordDTO);

    /**
     * 修改用户手机号
     * @param updatePhoneDTO 用户id、新手机号、更新时间
     */
    @Update("update user set phone = #{newPhone}, update_time = #{updateTime} where id = #{id}")
    void updatePhone(UpdatePhoneDTO updatePhoneDTO);
}
