package com.xpq.petserver.mapper;

import com.xpq.petserver.entity.DO.BailmentData;
import com.xpq.petserver.entity.DTO.PublicFosterageDTO;
import com.xpq.petserver.entity.DTO.SearchBailmentDataDTO;
import com.xpq.petserver.entity.VO.BailmentDetailVO;
import com.xpq.petserver.entity.VO.MyFosterageVO;
import com.xpq.petserver.entity.VO.SearchBailmentDataVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * BailmentData相关操作
 * @author XPQ
 * @date 2022-04-01
 */
@Repository
public interface BailmentDataMapper {
    /**
     * 添加数据
     * @param publicFosterageDTO
     */
    @Insert("insert into bailment_data "
            + "(id, title, pet_type,pet_sex, pet_month, linkman, phone, wechat, address, description, user_id, create_time, update_time) "
            + "values (#{id},#{title},#{petType},#{petSex},#{petMonth},#{linkman},#{phone},#{wechat},#{address},#{description},#{userId},#{createTime},#{updateTime})")
    void insertBailmentData(PublicFosterageDTO publicFosterageDTO);

    /**
     * 查询用户的寄养数据
     * @param userId 用户id
     * @return
     */
    @Select("select id, title, pet_type,pet_sex, pet_month, create_time from bailment_data where user_id = #{userId}")
    List<MyFosterageVO> selectMyFosterage(@Param("userId") String userId);

    @Select("select * from bailment_data where id = #{id}")
    BailmentDetailVO selectBailmentDetail(@Param("id") Long id);

    /**
     * 删除某条寄养数据
     * @param id 寄养数据id
     */
    @Delete("delete from bailment_data where id = #{id}")
    void deleteBailmentData(@Param("id") Long id);

    /**
     * 查询某一条寄养数据
     * @param id 寄养数据id
     * @return
     */
    @Select("select * from bailment_data where id = #{id}")
    BailmentData selectBailmentData(@Param("id") Long id);

    /**
     * 查询搜索的寄养数据
     * @param searchBailmentDataDTO
     * @return
     */
    List<SearchBailmentDataVO> selectSearchBailmentDataVO(SearchBailmentDataDTO searchBailmentDataDTO);


}
